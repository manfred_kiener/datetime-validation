﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Datetime.Models
{
    public class Test
    {
        [DataType(DataType.Date)]
        [Required]
        public DateTime Datum_1_Pflichteingabe { get; set; }

        [DataType(DataType.Date)]
        public DateTime? Datum_2_Optional { get; set; }
    }
}