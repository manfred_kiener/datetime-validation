﻿using Datetime.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Datetime.Controllers
{
    public class TestController : Controller
    {
        //
        // GET: /Test/
        public ActionResult Index()
        {
            var test = new Test();
            return View(test);
        }

        [HttpPost]
        public ActionResult Save(Test test)
        {
            Debugger.Break();
            return Json(new { UserMessage = "", Success = true });
        }
	}
}